#include <iostream>
#include <bits/stdc++.h>
#include <chrono>
#include "Graph.h"

using namespace std;
using namespace std::chrono;

// Function to check if file specified by its path (name) exists
bool isFile(char* fileName){
    string fName = string(fileName);
    ifstream f(fName);
    return f.good();
}

// Function to write out program help info
void writeHelp(ostream& os){
    os << "Help page for all-pairs shortest paths program usage." << endl;
    os << "This output is generated using argument --help (-h)." << endl;
    os << "To run program in serial version use command" << endl;
    os << "\t./sem <INPUT_FILE> <OUTPUT_FILE>" << endl;
    os << "To run parallel version add --parallel or -p argument, e.g." << endl;
    os << "\t./sem --parallel (-p) <INPUT_FILE> <OUTPUT_FILE>" << endl;
    os << "If OUTPUT_FILE is not specified standard output is used instead." << endl;
}

int main(int argc, char* argv[]) {
    if (argc == 1){
        cout << "Wrong program arguments. Run with '--help' for more information." << endl;
    }

    else if (argc == 2 && (!strcmp(argv[1], "--help") || !strcmp(argv[1], "-h"))){
        writeHelp(cout);
        return 0;
    }

    else if (argc >= 3 && (!strcmp(argv[1], "--parallel") || !strcmp(argv[1], "-p")) && isFile(argv[2])){
        cout << "Running parallel version of program..." << endl;
        string inputFile = string(argv[2]);
        ifstream in(inputFile);
        cin.rdbuf(in.rdbuf());
        int N, M;
        cin >> N >> M;

        Graph graph(N,M);
        graph.readInput(cin);
        auto start = high_resolution_clock::now();
        graph.computeShortestPathsParallel();
        auto runtime = duration_cast<duration<double>>(high_resolution_clock::now()-start).count();
        cout << "Computational time of All-pairs shortest paths: " << runtime << " s." << endl;
        if (argc >= 4){
            string outputFile = string(argv[3]);
            graph.printAllShortestPaths(outputFile);
        }
        else{
            graph.printAllShortestPaths(cout);
        }
    }

    else if (argc >= 2 && (strcmp(argv[1], "--parallel") != 0 && strcmp(argv[1], "-p") != 0) && isFile(argv[1])){
        cout << "Running serial version of program..." << endl;
        string inputFile = string(argv[1]);
        ifstream in(inputFile);
        cin.rdbuf(in.rdbuf());
        int N, M;
        cin >> N >> M;

        Graph graph(N,M);
        graph.readInput(cin);
        auto start = high_resolution_clock::now();
        graph.computeShortestPaths();
        auto runtime = duration_cast<duration<double>>(high_resolution_clock::now()-start).count();
        cout << "Computational time of All-pairs shortest paths: " << runtime << " s." << endl;
        if (argc >= 3){
            string outputFile = string(argv[2]);
            graph.printAllShortestPaths(outputFile);
        }
        else{
            graph.printAllShortestPaths(cout);
        }
    }
    else{
        cout << "Wrong program arguments. Run with '--help' for more information." << endl;
    }
    return 0;
}
