#include <iostream>
#include <bits/stdc++.h>
#include <random>

using namespace std;

int main(int argc, char* argv[]) {
    if (argc != 2){
        cout << "Specify output file." << endl;
        return 0;
    }
    // Read input arguments defining size of graph
    int N, M;
    cin >> N >> M;

    // Open file for generated instance and write out size of graph
    ofstream file;
    file.open(argv[1]);
    file << N << " " << M << endl;

    // Simple checker for already generated edges (not to have parallel edges)
    vector<vector<bool>> existingEdge(N, vector<bool>(N, false));

    // Initialize random number generators for both integers (vertices) and doubles (edge costs)
    random_device rd;
    mt19937 generator(rd());
    uniform_int_distribution<int> vDistribution(0,N-1);
    uniform_real_distribution<double> costDistribution(0., 50.);

    // Generate M edges
    for (int i = 0; i < M; i++) {
        int src = vDistribution(generator);
        int dst = vDistribution(generator);
        double cost = costDistribution(generator);

        // If generators outputs the same vertex as a source and destination (positive cycle in vertex)
        // or there already exists edge from source to destination -> skip and re-generate edge
        if (src == dst || existingEdge[src][dst]) {
            i--;
            continue;
        }

        // Write out generated edges
        file << src << " " << dst << " " << cost << endl;
        existingEdge[src][dst] = true;
    }

    // Close output instance file and exit
    file.close();
    return 0;
}
