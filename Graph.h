#include <vector>
#include <sstream>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <string>
#ifndef SEM_GRAPH_H
#define SEM_GRAPH_H

#define NUM_THREADS 8

using namespace std;
class Graph {
public:
    Graph(int N, int M);
    ~Graph() = default;
    void readInput(istream& is);
    void computeShortestPaths();
    void computeShortestPathsParallel();
    void printAllShortestPaths(ostream& os);
    void printAllShortestPaths(const string& fileName);
private:
    void wait();
    void parallelization(int thread_id);
    vector<vector<double>> adjacencyMatrix;
    vector<vector<int>> throughMatrix;
    int N;
    int M;
    bool negativeCycles;
    // Variables for barrier implementation for parallel algorithm
    atomic_int mFreeThreadSlots;
    long mPhaseCounter = 0;
    int mNumThreads = NUM_THREADS;
    mutex mMutex;
    condition_variable mCondVar;
};


#endif //SEM_GRAPH_H
