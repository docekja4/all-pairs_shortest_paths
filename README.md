# Nejkratší cesty v grafu mezi všemi vrcholy

Zadání semestrální práce je vytvořit program řešící problém nalezení nejkratších cest v grafu mezi všemi vrcholy.

Pro tento problém je nejlepším algoritmem **Floyd-Warshallův algoritmus**. Tento je implementován jak v sériové verzi, tak v paralelní verzi.

Floyd-Warshallův algoritmus používá princip dynamického programování, kdy iterativně zlepšuje možné cesty mezi všemi dvojicemi vrcholů přes jiný vrchol k. K tomu používá 2D matici o velikosti NxN, kde N je počet vrcholů grafu a je inicializovaná jako takzvaná matice sousednosti. To znamená, že na příslušných indexech i,j je cena hrany z vrcholu i do vrcholu j. Pokud taková hrana neexistuje, určíme cenu jako nekonečno.

Paralelní verze používá C++ knihovnu pro práci s vlákny spolu s dalšími standardními knihovnami, které jsou použity i v sériové (jednovláknové) implementaci. Paralelní verze je implementována pomocí takzvané bariéry. Aby byla vlákna správně synchronizována, je nutné aby v kritické sekci kódu na sebe počkala. Poté může algoritmu pokračovat další iterací. Tento přístup přesně splňuje bariéra, kdy všechna vlákna čekají, dokud i poslední z nich není hotové s výpočtem.

### Použití
Po zkompilování pomocí přiloženého *CMakeLists.txt* souboru lze program pustit například pomocí příkazu
```
./sem <INPUT_FILE> <OUTPUT_FILE>
```
Tento spustí sériovou verzi pro výpočet nejkratších cest grafu načteného se vstupního souboru (INPUT\_FILE) a vypíše výsledek do výstupního souboru (OUTPUT\_FILE). Argument OUTPUT_FILE je volitelný, pokud jej vynecháme, výsledek se vypíše na standardní výstup.
Paralelní verzi lze pustit přidáním přepínače `--parallel`. Konkrétní popis spouštění lze vypsat pomocí
```
./sem --help
```
### Formát vstupního souboru
Soubor se vstupní instancí grafu začíná informací o počtu vrcholů N a počtu hran M grafu. Na dalších M řádcích souboru jsou specifikovány hrany ve tvaru `source destination cost`.

Pro **generování** vstupních instancí je přiložen kód, který je též kompilovaný přiloženým *CMakeLists.txt* souborem. Po spuštění program čeká na specifikaci velikosti instance. První čte číslo N (počet vrcholů grafu) a jako druhé čte číslo M (počet hran grafu). Vygenerovaný graf zapíše v požadovaném formátu do specifikovaného výstupního souboru.
```
./instanceGenerator <OUTPUT_FILE>
```
### Měření rychlosti
Program byl spouštěn na procesoru i5-7200U. Jedná se o 2 jádrový procesor taktován na 2.50 GHz. Program byl pouštěn ve verzi commitu 7253777203e. Pro měření a porovnání času byl použit graf s 1000 vrcholy a s 2500 vrcholy (konkrétně instance 1000\_700000.txt a 2500\_1000000.txt v přiložených instancích).
```
Serial version for N = 1000 took 1.431 s.
Parallel version for N = 1000 took 0.819 s.
Serial version for N = 2500 took 20.482 s.
Parallel version for N = 2500 took 10.849 s.
```
