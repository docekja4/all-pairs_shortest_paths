#include <limits>
#include <iostream>
#include <thread>
#include <bits/stdc++.h>
#include "Graph.h"

// Two debugging functions for printing class matrices, not optimized in terms of output format, but not necessary
void print_matrix(const vector<vector<double>> &mat){
    for (size_t i = 0; i < mat.size(); i++){
        for (size_t j = 0; j < mat.size(); j++){
            cout << mat[i][j] << " ";
        }
        cout << endl;
    }
}
void print_matrix(const vector<vector<int>> &mat){
    for (size_t i = 0; i < mat.size(); i++){
        for (size_t j = 0; j < mat.size(); j++){
            cout << mat[i][j] << " ";
        }
        cout << endl;
    }
}

// Graph class constructor initializing graph size and corresponding matrices later used for shortest paths
Graph::Graph(int N, int M) : N(N), M(M){
    mFreeThreadSlots = NUM_THREADS;
    adjacencyMatrix = vector<vector<double>> (N, vector<double>(N, numeric_limits<double>::infinity()));
    for (int i = 0; i < N; i++){
        adjacencyMatrix[i][i] = 0.0;
    }
    throughMatrix = vector<vector<int>> (N, vector<int>(N, -1));
}

// Simple parser of input stream (file)
void Graph::readInput(istream& is) {
    int src, dst;
    double C;
    for (size_t i = 0; i < M; i++){
        is >> src >> dst >> C;
        // Add corresponding information to our matrices
        adjacencyMatrix[src][dst] = C;
        throughMatrix[src][dst] = dst;
    }
}

void Graph::computeShortestPaths() {
    // Classic serial version of Floyd-Warshall algorithm for all-pairs shortest paths
    for (size_t k = 0; k < N; k++){
        for (size_t i = 0; i < N; i++){
            for (size_t j = 0; j < N; j++){
                // If there is improvement on path from 'i' to 'j' through vertex 'k', update adjacency matrix
                // and save this vertex 'k' as a better to go through
                if (adjacencyMatrix[i][j] > adjacencyMatrix[i][k] + adjacencyMatrix[k][j]){
                    adjacencyMatrix[i][j] = adjacencyMatrix[i][k] + adjacencyMatrix[k][j];
                    throughMatrix[i][j] = throughMatrix[i][k];
                }
            }
        }
    }

    // If graph contains negative cycle, there will be non-zero (negative) costs on diagonal
    for (int i = 0; i < N; i++){
        if (adjacencyMatrix[i][i] != 0){
            negativeCycles = true;
        }
    }
    negativeCycles = false;
}

void Graph::printAllShortestPaths(ostream& os) {
    // If graph contains negative cycles, output it to file
    if (negativeCycles){
        os << "Graph contains negative cycles." << endl;
        return;
    }
    int a,b;
    double cost;
    // Iterate over all pairs of vertices in graph
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            // If there exist path between such pair of vertices, reconstruct it and output it
            if (i != j && adjacencyMatrix[i][j] != numeric_limits<double>::infinity()) {
                a = i;
                b = j;
                vector<int> path;
                os << "Path from " << a << " to " << b << " is: ";
                path.push_back(a);
                cost = adjacencyMatrix[a][b];       // obtain cost of path, already stored in adjacency matrix
                while (a != b) {
                    a = throughMatrix[a][b];        // move to next best vertex on path until we "arrive" to the goal
                    path.push_back(a);
                }
                // Print path to output stream with information about cost
                for (size_t k = 0; k < path.size(); k++) {
                    os << path[k];
                    if (k != path.size() - 1) os << " -> ";
                }
                os << " with cost: " << cost << "." << endl;
            }
        }
    }
}


void Graph::wait() {
    // Save phase of current waiting thread
    auto currentPhase = mPhaseCounter;

    // If "last" thread calls wait() function, we can release barrier and reset it
    if (--mFreeThreadSlots == 0) {
        // Reset.
        mFreeThreadSlots = mNumThreads;
        {
            // Locking is necessary, because if some thread is currently inside mCondVar predicate, notify_all()
            // will not signal it again (it may have old mPhaseCounter value).
            unique_lock<mutex> lock(mMutex);
            mPhaseCounter++;
        }

        // Notify all threads that they are released
        mCondVar.notify_all();
        return;
    }
    else {
        // Thread is in waiting mode, waiting for "last" thread to finish their computation,
        // which will be announced by conditional variable
        unique_lock<mutex> myLock(mMutex);
        mCondVar.wait(myLock, [&currentPhase, this]{
            return currentPhase < mPhaseCounter;
        });
    }
}

void Graph::parallelization(int thread_id) {
    // Simple parallelization of Floyd-Warshall algorithm
    for (size_t k = 0; k < N; k++){
        // Only two inner loops can be parallelized due to dynamic programming approach via 'k' iteration
        // Rows of adjacency matrix are distributed between threads
        for (size_t i = thread_id; i < N; i+=NUM_THREADS){
            for (size_t j = 0; j < N; j++){
                if (adjacencyMatrix[i][j] > adjacencyMatrix[i][k] + adjacencyMatrix[k][j]){
                    adjacencyMatrix[i][j] = adjacencyMatrix[i][k] + adjacencyMatrix[k][j];
                    throughMatrix[i][j] = throughMatrix[i][k];
                }
            }
        }
        // Wait for all threads to finish their computing using Barrier approach
        wait();
    }
}

void Graph::computeShortestPathsParallel() {
    // Create defined number of threads for all-pairs shortest paths computation
    vector<thread> threads;
    for (int i = 0; i < NUM_THREADS; i++){
        threads.push_back(thread(&Graph::parallelization, this, i));
    }

    // After all computing is done, close threads
    for (auto& worker : threads){
        worker.join();
    }

    // If graph contains negative cycle, there will be non-zero (negative) costs on diagonal
    for (int i = 0; i < N; i++){
        if (adjacencyMatrix[i][i] != 0){
            negativeCycles = true;
        }
    }
    negativeCycles = false;
}

void Graph::printAllShortestPaths(const string& fileName) {
    // Open output file
    ofstream file;
    file.open(fileName);
    // If graph contains negative cycles, output it to file
    if (negativeCycles){
        file << "Graph contains negative cycles." << endl;
        file.close();
        return;
    }
    int a,b;
    double cost;
    // Iterate over all pairs of vertices in graph
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            // If there exist path between such pair of vertices, reconstruct it and output it
            if (i != j && adjacencyMatrix[i][j] != numeric_limits<double>::infinity()) {
                a = i;
                b = j;
                vector<int> path;
                file << "Path from " << a << " to " << b << " is: ";
                path.push_back(a);
                cost = adjacencyMatrix[a][b];       // obtain cost of path, already stored in adjacency matrix
                while (a != b) {
                    a = throughMatrix[a][b];        // move to next best vertex on path until we "arrive" to the goal
                    path.push_back(a);
                }
                // Print path to output stream with information about cost
                for (size_t k = 0; k < path.size(); k++) {
                    file << path[k];
                    if (k != path.size() - 1) file << " -> ";
                }
                file << " with cost: " << cost << "." << endl;
            }
        }
    }
    // Close output file
    file.close();
}
